from PyQt6 import QtWidgets
import sys
from pass_catcha import Ui_MainWindow
import threading
import time
from chorme import run_chorme
import numpy as np
import requests
class Main_code:
  def run(self):
    #setup
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    self.ui = Ui_MainWindow()
    self.ui.setupUi(MainWindow)

    #Goi ham get setting 
    self.get_setting()

    #Bat su kien click luu cai dat
    self.ui.button_save.clicked.connect(self.click_btn_save)

    #Bat su kien click button start
    self.ui.button_start.clicked.connect(self.click_btn_start_ver2)

    #open
    MainWindow.show()
    # bat su kien close
    sys.exit(app.exec())

  def get_setting(self):
    file = open('setting.txt', 'r+')
    read_data = file.read()
    self.setting = read_data.split('\n')

    self.ui.inout_gpm.setText(self.setting[0])
    # self.ui.input_captcha.setText(self.setting[1])

    file.close()

  def click_btn_save(self):
    gpm = self.ui.inout_gpm.text()
    # key_captcha = self.ui.input_captcha.text()

    #Lưu lại biến golbal
    data_save = [gpm]
    self.setting = data_save

    #Luu lai vao file setting
    file = open('setting.txt', 'r+')
    file.write(data_save[0] + '\n' + data_save[1])
    file.close()

  def click_btn_start(self):
    global count
    #lay du lieu tu 2 file acc proxy
    list_acc = self.get_acc()
    list_proxy = self.get_proxy()

    print(list_acc)
    print(list_proxy)

    list_acc_chuck = self.chuck(list_acc, len(list_proxy))
    
    #xu ly du lieu chay da luong
    count = 0
    def loop():
      global count
      if count < len(list_acc_chuck): # =2
        list_thread = []

        for index, item in enumerate(list_acc_chuck[count]): #['acc1|pass1', 'acc2|pass2', 'acc3|pass3', 'acc4|pass4']
          # index 0, 1, 2, 3
          # item acc1|pass1 acc2
          username = item.split('|')[0]
          password = item.split('|')[1]
          thread = threading.Thread(target=self.item_run, args=(username, password, list_proxy[index], self.setting[0],))
          list_thread.append(thread)

        for item_thread in list_thread:
          item_thread.start()

        while True:
          my_threading = threading.active_count() 
          
          if my_threading == 1:
            count = count + 1
            time.sleep(10)
            loop()
            break
          else:
            continue

    loop()

  def click_btn_start_ver2(self):
    global count
    #lay du lieu tu 2 file acc proxy
    list_acc = self.get_acc()
    list_proxy = self.get_proxy()

    arr = np.array(list_acc)

    list_acc_chuck = np.array_split(arr, len(list_proxy))

    list_thread = [] # 4 luong
    
    for index, item in enumerate(list_acc_chuck):
      thread = threading.Thread(target=self.item_run, args=(item, list_proxy[index],))
      list_thread.append(thread)

    for item_thread in list_thread:
      item_thread.start()


        

  def item_run(self, list, key_proxy):
    for item in list:
      username = item.split('|')[0]
      password = item.split('|')[1]

      print(key_proxy)

      proxy = self.call_api_proxy(key_proxy)
      print(proxy)
      # proxy = '192.168.1.1'

      run = run_chorme(username, password, proxy, self.setting[0])
      run.run()

      time.sleep(10)

  def call_api_proxy(self, key_proxy):
    res = requests.get('http://proxy.tinsoftsv.com/api/changeProxy.php?key=' + key_proxy)
    data = res.json()

    if data['success']:
      return data['proxy']
    else:
      res_hien_tai = requests.get('http://proxy.tinsoftsv.com/api/getProxy.php?key=' + key_proxy)
      data_hien_tai = res_hien_tai.json()

      if data_hien_tai['success']:
        return data_hien_tai['proxy']
      
  def get_acc(self):
    file = open('acc.txt', 'r+')
    data = file.read()
    file.close()
    return data.split('\n')
  
  def get_proxy(self):
    file = open('proxy.txt', 'r+')
    data = file.read()
    file.close()
    return data.split('\n')
  
  #Chia list => cac list dung so luong cua proxy
  def chuck(self, list, soluong_proxy):
    if not list:
      return []
    return [list[:soluong_proxy]] + self.chuck(list[soluong_proxy:], soluong_proxy)


main = Main_code()
main.run()