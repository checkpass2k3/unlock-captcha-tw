import time
from GPMLoginAPI import GPMLoginAPI
from selenium import webdriver
# python3 -m pip install --upgrade pip
# python3 -m pip
# pip install selenium
from selenium.webdriver.chrome import service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class run_chorme:
  def __init__(self, username, password, proxy, gpm):
    self.username = username
    self.password = password
    self.proxy = proxy
    self.gpm = gpm
    # self.key_captcha = key_captcha

  def run(self):
    api = GPMLoginAPI(self.gpm) # Alert: copy url api on GPM Login App
    createdResult = api.Create(name="profile" + self.username, proxy=self.proxy)
    profileId = str(createdResult['profile_id'])
    try:
      startedResult = api.Start(profileId=profileId, addinationArgs="--start-maximized")

      time.sleep(2)

      chrome_options = Options()
      chrome_options.add_argument("--lang=en-US")
      chrome_options.add_experimental_option("debuggerAddress", startedResult["selenium_remote_debug_address"])

      chrome_options.arguments.extend(["--no-default-browser-check", "--no-first-run"])

      chrome_options.add_argument("--verbose")  # Enable verbose logging
      chrome_options.add_argument("--log-path=D:\\gpmdriver.log")  # Set the path to the log file

      driver_path = startedResult["selenium_driver_location"]
      # Set the desired log level to capture all logs
      desired_capabilities = DesiredCapabilities.CHROME.copy()


      ser = Service(driver_path)

      self.driver = webdriver.Chrome(service=ser, options=chrome_options) #, desired_capabilities=desired_capabilities)
      # input("Enter to go gleam")
      self.driver.get("https://vn.tbab953.com/Home/Index")
      
      time.sleep(5)
      btn_login = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[1]/div[1]/div[3]/a[2]')
      btn_login.click()

      # time.sleep(3)
      wait = WebDriverWait(self.driver, 10) # hàm cài đặt chờ element xuất hiện
      input_email = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[13]/div/div/div/div/div/div[2]/div/form/ul/li[1]/div/input')))
      input_email.send_keys(self.username)

      input_password = self.driver.find_element(By.XPATH, '/html/body/div[13]/div/div/div/div/div/div[2]/div/form/ul/li[2]/div/input')
      input_password.send_keys(self.password)
      input_password.send_keys(Keys.ENTER)

      try:
        name_user = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[1]/div[1]/div[1]/div[3]/ul/li[2]/span[1]')))
        print('Login success:', name_user.text)
        self.save_success(self.username, self.password)

        self.driver.quit()

        api.Stop(profileId)
        time.sleep(1)

        print('Delete profile', self.username)
        api.Delete(profileId)
      except:
        print('Login fail')
        self.save_fail(self.username, self.password)
        self.driver.quit()

        api.Stop(profileId)
        time.sleep(1)

        print('Delete profile', self.username)
        api.Delete(profileId)
      # time.sleep(7)
        

      
      # try:
      #   btn_continue = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/form/input[5]')))
      #   btn_continue.click()
      # except:
      #   print('vuot qua continue')

      # time.sleep(3)
      # try:
      #   btn_start = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/form/input[6]')))
      #   btn_start.click()
      # except:
      #   print('pass start')

      # time.sleep(5)
      # iframe = wait.until(EC.visibility_of_element_located((By.ID,'arkose_iframe')))
      # iframe_src = iframe.get_attribute("src")

      # key_site = iframe_src.split('/')[3]
        
      # done = wait.until(EC.visibility_of_element_located((By.XPATH,'/html/body/div[2]/div/form/input[6]')))
      # done.click()

      # self.driver.quit()

      # api.Stop(profileId)
      # time.sleep(1)

      # print('Delete profile', self.username)
      # api.Delete(profileId)

      # print('Done')
    except:
      self.driver.quit()

      api.Stop(profileId)
      time.sleep(1)

      print('Delete profile', self.username)
      api.Delete(profileId)

      print('Loi')

    #/html/body/div[2]/div/form/input[6]

  def get_funcaptcha(self, key_site):
    apikey = self.key_captcha
    client = OneStCaptchaClient(apikey=apikey)
    site_key = key_site
    site_url = "https://twitter.com/"
    while True:
        result = client.fun_captcha_task_proxyless(site_url  , site_key)
        # result= client.image_to_text(base64img="base64img") # if you send base64img
        print(result)
        if result["code"] == 0:  # success:
            return result["token"]
        else:  # wrong
            time.sleep(5)
            continue
        
  def save_success(self, username, password):
    with open('success.txt', 'a') as f:
      f.write(username + '|' + password + '\n')
      f.close()

  def save_fail(self, username, password):
    with open('fail.txt', 'a') as f:
      f.write(username + '|' + password + '\n')
      f.close()